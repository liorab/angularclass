import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {

  constructor(public authService:AuthService, private router:Router) { }
  
  email:string;
  password:string;

  ngOnInit() {
  }
  onSubmit(){
    this.authService.login(this.email,this.password);
    //זה קורה בצורה סינכרונית והמשתמש בצורה אסינכרונית ובגלל זה יש בעיות
    //this.router.navigate(['/books']);
  }

}

 