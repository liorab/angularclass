import { AuthService } from './../auth.service';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';




@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {



  title:string = "Welcome";
  panelOpenState=false;
  //books:any;
  books$:Observable<any>;
  userId:string;

  deleteBook(id:string,userId:string){
    this.booksservise.deleteBook(id,this.userId);
  }

  constructor(private booksservise:BooksService,
              public authService:AuthService) { }

  ngOnInit() {
    /*
    this.books=this.booksservise.getBooks().subscribe(
      (books)=>this.books = books
    );
    */
    // this.booksservise.addBooks();
   //this.books$=this.booksservise.getBooks();
      this.authService.user.subscribe(
        user => {
          this.userId = user.uid;
          this.books$ = this.booksservise.getBooks(this.userId);        }
      )
  }

}
