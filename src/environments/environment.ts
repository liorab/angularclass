// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyANM_9Qx9JBuI1NK1DpiE0TJhd3HpGTMUs',
    authDomain: 'angular-901f1.firebaseapp.com',
    databaseURL: 'https://angular-901f1.firebaseio.com',
    projectId: 'angular-901f1',
    storageBucket: 'angular-901f1.appspot.com',
    messagingSenderId: '1011035654341',
    appId: '1:1011035654341:web:c30a6c93b19c0244c608ac',
    measurementId: 'G-JDDFNQXCJN'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
